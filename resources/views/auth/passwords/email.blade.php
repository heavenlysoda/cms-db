@extends('layouts.header')
<body>
    <div class="login-container">
        <div class="login-box animated fadeInDown">
            @if (session('status'))
                <div class='row'>
                    <div class='alert alert-success'>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <center>{{ session('status') }}</center>
                    </div>
                </div>
            @endif
            <div class="login-logo"></div>
            <div class="login-body">
                <div class="login-title"><strong>Forgot Password</strong><br>Insert Your Email Here!</div>
                   

                    <form class="form-horizontal" method="POST" role="form" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" placeholder="Your E-mail Address" name="email" value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6">
                                <a href="{{ url('/login') }}" class="btn btn-link btn-block">Back to Login Page</a>
                            </div>
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-info btn-block">Submit</button>
                            </div>
                        </div>
                    </form>
            </div>
            <div class="login-footer">
                <div class="pull-left">
                    Copyright &copy; {{ date('Y') }} Reliance Group
                </div>
               
            </div>
            </div>
        </div>
    </div>
</body>
</html>

