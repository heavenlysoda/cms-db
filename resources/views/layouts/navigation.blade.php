           <!-- START X-NAVIGATION -->
            <ul id="mymenu" class="x-navigation">
                <li class="xn-logo">
                    <a href="/dashboard">{{-- <img src="{{ asset('img/logo.png') }} "> --}}Reliance Group</a>
                    <a href="#" class="x-navigation-control"></a>
                </li>
                <li class="xn-profile">
                    <a href="#" class="profile-mini">
                        <img src="{{asset('assets/images/users/no-image.jpg')}}"/>
                    </a>
                    <div class="profile">
                        <div class="profile-image">
                            <img src="{{ asset('assets/images/users/no-image.jpg') }}" />
                        </div>
                        <div class="profile-data">
                            <div class="profile-data-name"></div>
                            <div class="profile-data-title">Administrator</div>
                        </div>
                        {{-- <div class="profile-controls">
                            <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                            <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                        </div> --}}
                    </div>                                                                        
                </li>
                    <!-- <li class="xn-title">Navigation</li> -->
                    <li>
                        <a href="/dashboard"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
                    </li>
                    <li>
                        <a href="{{ url('admin/dashboard') }}"><span class="fa fa-desktop"></span> <span class="xn-text">Customer</span></a>                        
                    </li>
                    <li class="xn-openable">
                        <a href="{{ url('admin/admin') }}"><span class="fa fa-user"></span> <span class="xn-text">Business Unit</span></a>
                        <ul>
                            <li><a href="{{ url('admin/admin') }}"><span class="fa fa-list-ul"></span> RELI</a></li>
                            <li><a href="{{ url('admin/admin/create') }}"><span class="fa fa-list-ul"></span> RMI</a></li>
                            <li><a href="{{ url('admin/admin/create') }}"><span class="fa fa-list-ul"></span> ARI GEN</a></li>
                            <li><a href="{{ url('admin/admin/create') }}"><span class="fa fa-list-ul"></span> ARI HEALTH</a></li>
                            <li><a href="{{ url('admin/admin/create') }}"><span class="fa fa-list-ul"></span> AJRI</a></li>
                            <li><a href="{{ url('admin/admin/create') }}"><span class="fa fa-list-ul"></span> REFI</a></li>
                        </ul>                    
                    </li>

                    <li class="xn-openable">
                        <a href="{{ url('admin/user') }}"><span class="fa fa-users"></span> <span class="xn-text">User Management</span></a>
                        <ul>
                            <li><a href="{{ url('admin/user') }}"><span class="fa fa-list-ul"></span> List User</a></li>    
                            <li><a href="{{ url('admin/user/suspend') }}"><span class="fa fa-users"></span> Suspended User</a></li>                        
                            <li><a href="{{ url('admin/user/create') }}"><span class="fa fa-plus"></span> Add New User</a></li>
                        </ul>                   
                    </li>

{{--                     <li class="xn-openable">
                        <a href="{{ url('admin/event') }}"><span class="fa fa-tasks"></span> <span class="xn-text">Events</span></a>
                        <ul>
                            <li><a href="{{ url('admin/event') }}"><span class="fa fa-list-ul"></span> List Event</a></li>
                        </ul>
                        <ul>
                            <li><a href="{{ url('admin/event/create') }}"><span class="fa fa-plus"></span> Add New Event</a></li>
                        </ul>
                        <ul>
                            <li><a href="{{ url('admin/document/create') }}"><span class="fa fa-upload"></span> Upload Document</a></li> 
                        </ul> 
                    </li> --}}
{{--                     <li class="xn-openable">
                        <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Resources</span></a>
                        <ul>
                            <li><a href="{{ url('admin/category') }}"><span class="fa fa-list-ul"></span> Category</a></li>
                        </ul>
                        <ul>
                            <li><a href="{{ url('admin/category/create') }}"><span class="fa fa-plus"></span> Add New Category</a></li> 
                        </ul>
                        <ul>
                            <li><a href="{{ url('admin/file/create') }}"><span class="fa fa-upload"></span> Upload File</a></li> 
                        </ul> 
                    </li> --}}

            </ul>