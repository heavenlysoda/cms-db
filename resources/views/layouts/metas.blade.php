<!-- GENERAL META -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="description" content="ICSA is a website based for the company." />


<!-- Open Graph data -->


<meta property="og:site_name" content="ICSA & FSA Indonesia Corporate Secretary Association" />

<meta property="og:description" content="ICSA is a website based for the company." />

<!-- <meta property="og:image" content="{{ asset('assets/img/fb.png') }}" /> -->

<meta property="og:type" content="website" />

<!-- TITLE -->

<!-- ICON -->
<!-- <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon" /> -->
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">

<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">

<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">

<link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}" />

<link rel="manifest" href="/manifest.json">

<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">

<meta name="theme-color" content="#ffffff">