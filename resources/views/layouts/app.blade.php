<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('meta')
    
    <title>Reliance Group</title>
    
    <link rel="stylesheet" type="text/css" id="theme" href="{{ asset('css/theme-default.css') }}"/>
    
    <link href="{{url("assets/plugins/css/bootstrap-datepicker.min.css")}}" rel="stylesheet" type="text/css">
    <script src="{{url("assets/plugins/js/bootstrap-datepicker.min.js")}}"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/jquery/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/bootstrap/bootstrap.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/plugins.js') }}"></script>        
    <script type="text/javascript" src="{{ asset('js/actions.js') }}"></script>       


</head>
<body>
    <div class="page-container">
        <div id="mymenu" class="page-sidebar">
        @include('layouts.navigation')
        </div>
        <div class="page-content">
            <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                <li class="xn-icon-button">
                    <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                </li>
                <li class="xn-icon-button pull-right">
                    <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>
                </li>
            </ul>
            <ul class="breadcrumb">
                <!-- <li><a href="#">Home</a></li> {{-- href page home --}}
                <li class="active">Dashboard</li> -->
            </ul>
            <div class="page-content-wrap">
                {{-- @include('flash::message') --}}
                <div class="col-md-6 col-md-offset-3">
                    @if(session()->has('notif'))
                        <div class='row'>
                            <div class='alert alert-success'>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <center>{{ session()->get('notif') }}</center>
                            </div>
                        </div>
                    @elseif(session()->has('error'))
                        <div class='row'>
                                <div class='alert alert-danger'>
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <center>{{ session()->get('error') }}</center>
                                </div>
                        </div>
                    @endif
                </div>  
                @yield('content')
            </div>
        </div>
    </div>

    <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
        <div class="mb-container">
            <div class="mb-middle">
                <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                <div class="mb-content">
                    <p>Are you sure you want to log out?</p>                    
                    <p>Press No if you want to continue work. Press Yes to logout current user.</p>
                </div>
                <div class="mb-footer">
                    <div class="pull-right">
                        <a href="{{ URL::to('admin/logout') }}" 
                        onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="btn btn-success btn-lg">Yes</a>
                        <form id="logout-form" action="{{ URL::to('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                        <button class="btn btn-default btn-lg mb-control-close">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#flash-overlay-modal').modal();
    </script>      
    <script type='text/javascript' src="{{ asset('js/plugins/icheck/icheck.min.js') }}"></script>        
    <script type="text/javascript" src="{{ asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/scrolltotop/scrolltopcontrol.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/plugins/morris/raphael-min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('js/plugins/morris/morris.min.js') }}"></script>        -->
    <script type="text/javascript" src="{{ asset('js/plugins/rickshaw/d3.v3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/rickshaw/rickshaw.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>                
    <script type='text/javascript' src="{{ asset('js/plugins/bootstrap/bootstrap-datepicker.js') }}"></script>                
    <script type="text/javascript" src="{{ asset('js/plugins/owl/owl.carousel.min.js') }}"></script>                 

    <script type="text/javascript" src="{{ asset ('js/plugins/bootstrap/bootstrap-timepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset ('js/plugins/bootstrap/bootstrap-colorpicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset ('js/plugins/bootstrap/bootstrap-file-input.js') }}"></script>
    <script type="text/javascript" src="{{ asset ('js/plugins/bootstrap/bootstrap-select.js') }}"></script>
    <script type="text/javascript" src="{{ asset ('js/plugins/tagsinput/jquery.tagsinput.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/plugins/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- END THIS PAGE PLUGINS-->        
    <script type="text/javascript" src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <!-- START TEMPLATE -->
    <!-- <script type="text/javascript" src="{{ asset('js/settings.js') }}"></script> -->
    @yield('javascript')  

</body>

    
</html>